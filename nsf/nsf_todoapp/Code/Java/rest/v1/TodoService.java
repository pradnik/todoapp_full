package rest.v1;

import java.io.IOException;
import java.io.Reader;
import java.util.Vector;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import lotus.domino.Database;
import lotus.domino.Document;
import lotus.domino.NotesException;
import lotus.domino.View;
import lotus.domino.ViewEntry;
import lotus.domino.ViewNavigator;

import com.ibm.commons.util.io.json.JsonException;
import com.ibm.commons.util.io.json.JsonJavaFactory;
import com.ibm.commons.util.io.json.JsonJavaObject;
import com.ibm.commons.util.io.json.JsonParser;
import com.ibm.domino.services.ServiceException;
import com.ibm.domino.services.rest.RestServiceEngine;
import com.ibm.domino.services.util.JsonWriter;
import com.ibm.xsp.extlib.component.rest.CustomService;
import com.ibm.xsp.extlib.component.rest.CustomServiceBean;
import com.ibm.xsp.extlib.util.ExtLibUtil;

public class TodoService extends CustomServiceBean {
	public static final String BASE_PATH = "/todos";

	@Override
	public void renderService(CustomService service, RestServiceEngine engine)
			throws ServiceException {

		HttpServletRequest request = engine.getHttpRequest();
		HttpServletResponse response = engine.getHttpResponse();

		response.setHeader("Content-Type", "application/json; charset=UTF-8");
		String strMethod = request.getMethod();
		String strPathInfo = request.getPathInfo();
		try {
			if (strPathInfo.startsWith(BASE_PATH)) {
				if ("GET".equalsIgnoreCase(strMethod)) {

					if (strPathInfo.equalsIgnoreCase(BASE_PATH)) {
						// get all todos
						getTodos(response);
					} else {
						String[] elements = strPathInfo.split("/");
						if (elements.length > 1) {
							String id = elements[2];
							System.out.println(id);
							getTodo(Long.valueOf(id), response);
						} else {
							RestServiceEngine
									.writeJSONException(response.getWriter(),
											new Exception("Invalid Path"));
						}
					}
				} else if ("POST".equalsIgnoreCase(strMethod)) {
					addTodo(request, response);
				} else if ("PATCH".equalsIgnoreCase(strMethod)) {
					String[] elements = strPathInfo.split("/");
					if (elements.length > 1) {
						String id = elements[2];
						System.out.println(id);
						patchTodo(Long.valueOf(id), request, response);
					} else {
						RestServiceEngine.writeJSONException(response
								.getWriter(), new Exception("Invalid Path"));
					}
				} else if ("DELETE".equalsIgnoreCase(strMethod)) {
					String[] elements = strPathInfo.split("/");
					if (elements.length > 1) {
						String id = elements[2];
						System.out.println(id);
						deleteTodo(Long.valueOf(id), response);
					} else {
						RestServiceEngine.writeJSONException(response
								.getWriter(), new Exception("Invalid Path"));
					}
				} else {
					response.setStatus(501);
				}
			} else {
				RestServiceEngine.writeJSONException(response.getWriter(),
						new Exception("Invalid Path"));
			}
		} catch (Exception e) {
			try {
				RestServiceEngine.writeJSONException(response.getWriter(),e);
			} catch (IOException e1) {
				e1.printStackTrace();
			}
			e.printStackTrace();
		}

	}

	private void getTodo(Long id, HttpServletResponse response)
			throws NotesException, IOException, JsonException {
		Database db = getDb();
		View v = db.getView("todos");
		ViewEntry ve = v.getEntryByKey(id, true);
		JsonWriter jw = new JsonWriter(response.getWriter(), false);
		try {
			getTodoJson(ve, jw);
		} catch (NotesException e) {
			e.printStackTrace();
		}

		v.recycle();
		db.recycle();

		jw.close();
		response.setStatus(200);

	}

	private void deleteTodo(Long id, HttpServletResponse response)
			throws NotesException, IOException, JsonException {
		Database db = getDb();
		View v = db.getView("todos");
		Document doc = v.getDocumentByKey(id, true);
		if (doc != null) {
			doc.remove(true);
			response.setStatus(200);
		} else {
			response.setStatus(404);
		}

	}

	private void addTodo(HttpServletRequest request,
			HttpServletResponse response) throws NotesException, IOException,
			JsonException {
		Database db = getDb();
		View v = db.getView("todos");

		Document lastDoc = v.getLastDocument();
		Double id = 0.0;
		if (lastDoc != null) {
			id = lastDoc.getItemValueDouble("id") + 1;
		}

		JsonJavaFactory factory = JsonJavaFactory.instanceEx;
		Reader r = request.getReader();
		JsonJavaObject json = (JsonJavaObject) JsonParser.fromJson(factory, r);
		String text = json.getString("text");
		Boolean completed = json.getBoolean("completed");
		r.close();

		Document newTodo = db.createDocument();
		newTodo.replaceItemValue("form", "todo");
		newTodo.replaceItemValue("id", id);
		newTodo.replaceItemValue("text", text);
		newTodo.replaceItemValue("completed", completed ? "1" : "");
		newTodo.save(true, false);

		v.refresh();
		// response.setStatus(200);
		getTodo(id.longValue(), response);

	}

	private void patchTodo(Long id, HttpServletRequest request,
			HttpServletResponse response) throws NotesException, IOException,
			JsonException {
		Database db = getDb();
		View v = db.getView("todos");

		Document doc = v.getDocumentByKey(id, true);
		if (doc != null) {
			JsonJavaFactory factory = JsonJavaFactory.instanceEx;
			Reader r = request.getReader();
			JsonJavaObject json = (JsonJavaObject) JsonParser.fromJson(factory,
					r);

			if (json.containsKey("text")) {
				String text = json.getString("text");
				doc.replaceItemValue("text", text);
			}

			if (json.containsKey("completed")) {
				Boolean completed = json.getBoolean("completed");
				doc.replaceItemValue("completed", completed ? "1" : "");
			}
			
			
			r.close();
			doc.save(true, false);
			v.refresh();
			// response.setStatus(200);
			getTodo(id.longValue(), response);
			
		} else {
			response.setStatus(404);
		}
	}

	private void getTodoJson(ViewEntry ve, JsonWriter jw)
			throws NotesException, IOException, JsonException {
		Vector vals = ve.getColumnValues();
		jw.startObject();
		{
			jw.startProperty("id");
			jw.outLiteral(((Double) vals.get(0)).longValue());
			jw.endProperty();

			jw.startProperty("text");
			jw.outStringLiteral(((String) vals.get(1)));
			jw.endProperty();

			jw.startProperty("completed");
			jw.outBooleanLiteral(((String) vals.get(2)).equals("1"));
			jw.endProperty();
		}
		jw.endObject();

	}

	private void getTodos(HttpServletResponse response) throws NotesException,
			IOException, JsonException {
		Database db = getDb();
		View v = db.getView("todos");
		ViewNavigator vnav = v.createViewNav();

		JsonWriter jw = new JsonWriter(response.getWriter(), false);
		try {
			jw.startArray();
			ViewEntry ve = vnav.getFirst();
			while (ve != null) {
				ViewEntry nextVe = vnav.getNext();
				jw.startArrayItem();

				getTodoJson(ve, jw);

				jw.endArrayItem();
				ve.recycle();
				ve = nextVe;
			}
			jw.endArray();
		} catch (NotesException e) {
			e.printStackTrace();
		}

		v.recycle();
		db.recycle();

		jw.close();
		response.setStatus(200);

	}

	private Database getDb() {
		return ExtLibUtil.getCurrentDatabase();
	}
}
