import CreateTodo from '../components/create-todo'

import { connect } from 'react-redux'
import { addTodo } from '../actions'


const mapDispatchToProps = (dispatch) => {
  return {
    onSubmit: (text) => {
      dispatch(addTodo(text))
    }
  }
}

let CreateTodoContainer = connect(null, mapDispatchToProps)(CreateTodo)

export default CreateTodoContainer