import axios from 'axios';

//Fetch todos
export const REQUEST_TODOS = 'FETCH_TODOS';
export const RECEIVE_TODOS = 'FETCH_TODOS_SUCCESS';
// export const FETCH_TODOS_FAILURE = 'FETCH_TODOS_FAILURE';
// export const RESET_TODOS_POST = 'RESET_TODOS_POST';

const ROOT_URL = location.href.indexOf('localhost') > 0 ? 'http://localhost:3004' : document.getElementById("view:apiPath").value;

function requestTodos() {
    return {
        type:REQUEST_TODOS
    }
}

function receiveTodos(todos) {
    return {
        type:RECEIVE_TODOS,
        todos
    }
}

export function fetchTodos() {
    return dispatch => {
        dispatch(requestTodos()) //TODO - implement loading indicator
        return axios.get(`${ROOT_URL}/todos`)
            .then(response => dispatch(receiveTodos(response.data)));
    }
}

export function postNewTodo(todo) {
    

        return axios.post(`${ROOT_URL}/todos`,{
            text:todo.text,
            completed:todo.completed
            })
   
}

export function deleteTodoRest(id) {
    

        return axios.delete(`${ROOT_URL}/todos/${id}`)
   
}

export function updateTodo(id, text, completed) {
    

        return axios.patch(`${ROOT_URL}/todos/${id}`,{
            text:text,
            completed
            })
   
}