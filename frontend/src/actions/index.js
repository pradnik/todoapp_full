import { postNewTodo, fetchTodos, deleteTodoRest, updateTodo } from "./rest";

let nextTodoId = 1000 //temporary id
export const addTodo = (text) => {
  const todo = {
    id: nextTodoId++,
    text,
    completed: false
  }
  return dispatch => {
    dispatch({
      type: 'ADD_TODO',
      id: todo.id,
      text: todo.text
    });
    return postNewTodo(todo).then(()=>dispatch(fetchTodos()));
  }

}

export const deleteTodo = (id) => {
  return dispatch => {
    dispatch({
      type: 'DELETE_TODO',
      id
    });
    return deleteTodoRest(id).then(()=>dispatch(fetchTodos()));
  }

}

export const editTodo = (id) => {
  return {
    type: 'EDIT_TODO',
    id
  }
}

export const cancelEditTodo = (id) => {
  return {
    type: 'CANCEL_EDIT_TODO',
    id
  }
}

export const saveTodo = (id, text) => {
  return dispatch => {
    dispatch({
      type: 'SAVE_TODO',
      id,
      text
    });
    return updateTodo(id, text).then(()=>dispatch(fetchTodos()));
  }
}

export const setVisibilityFilter = (filter) => {
  return {
    type: 'SET_VISIBILITY_FILTER',
    filter
  }
}

export const toggleTodo = (id, completed) => {

  return dispatch => {
    dispatch({
      type: 'TOGGLE_TODO',
      id
    });
    return updateTodo(id, undefined, !completed).then(()=>dispatch(fetchTodos()));
  }
}