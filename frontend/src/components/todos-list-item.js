import React from 'react';
import { Button } from 'react-bootstrap';

export default class TodosListItem extends React.Component {

    renderTaskSection() {
        const {text, completed, id, isEditing} = this.props;
        const {onTodoClick} = this.props;
        const taskStyle = {
            color: completed ? 'green' : 'red',
            cursor: 'pointer'
        };
        if (isEditing) {
            return (
                 <div className="col-md-8">
                    <form onSubmit={this.saveChange.bind(this)}>
                        <input className="form-control" type="text" defaultValue={text} ref="editInput" />
                    </form>
                </div>
            )
        }
        return (
             <div className="col-md-8 todoText" style={taskStyle}
                onClick={() => (onTodoClick(id,completed))}
                >
                {text}</div>
        );
    }

    saveChange(event){
        const {id,  onSaveClick} = this.props;
        event.preventDefault();
        onSaveClick(id, this.refs.editInput.value);
    }

    renderActionsSection() {
        const {id, isEditing, onEditClick, onDeleteClick, onSaveClick, onCancelEditClick} = this.props;
        if (isEditing) {
            return (
               <div className="col-md-4">
                     <div className="btn-group" role="group">
                        <Button bsStyle="success" onClick={() => (onSaveClick(id, this.refs.editInput.value))}>Save</Button>
                        <Button bsStyle="danger" onClick={() => (onCancelEditClick(id))}>Cancel</Button>
                    </div>
                </div>
            );
        }
        return (
            <div className="col-md-4">
                <div className="btn-group-sm" role="group">
                    <Button onClick={() => (onEditClick(id))} >Edit</Button>
                    <Button className="todoDelete" onClick={() => (onDeleteClick(id))}>Delete</Button>
                </div>
            </div>
        );
    }
    render() {

        return (
            <div className="todoRow">
                {this.renderTaskSection()}
                {this.renderActionsSection()}
            </div>
        );
    }

}

TodosListItem.propTypes = {
    id: React.PropTypes.number.isRequired,
    text: React.PropTypes.string.isRequired,
    isEditing: React.PropTypes.bool,
    completed: React.PropTypes.bool.isRequired,

    onTodoClick: React.PropTypes.func.isRequired,
    onEditClick: React.PropTypes.func.isRequired,
    onDeleteClick: React.PropTypes.func.isRequired,
    onSaveClick: React.PropTypes.func.isRequired,
    onCancelEditClick: React.PropTypes.func.isRequired
}