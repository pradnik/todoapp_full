import _ from 'lodash';
import React from 'react';
import TodosListHeader from './todos-list-header';
import TodosListItem from './todos-list-item';


export default class TodosList extends React.Component {
    renderItems() {
        const props = _.omit(this.props, 'todos');

        return _.map(this.props.todos, (todo) => <TodosListItem key={todo.id} {...todo} {...props} />);
    }
    render() {

        return (
            <div className="row">

                <TodosListHeader />

                {this.renderItems()}

            </div>
        );
    }
}

TodosList.propTypes = {
    todos: React.PropTypes.array.isRequired
}