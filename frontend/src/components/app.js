import React from 'react';
import { connect } from 'react-redux';
import VisibleTodosList from '../containers/todos-list-container';
import CreateTodoContainer from '../containers/create-todo-container';
import { Navbar, MenuItem, NavDropdown, Nav, NavItem } from 'react-bootstrap';
import { fetchTodos } from "../actions/rest";


class App extends React.Component {

    componentDidMount() {
        const { dispatch } = this.props
        dispatch(fetchTodos())
    }

    renderNavbar() {
        return (
            <Navbar>
                <Navbar.Header>
                    <Navbar.Brand>
                        <a href="#">SUTOL 2016 automation demo</a>
                    </Navbar.Brand>
                </Navbar.Header>
                <Nav>
                    <NavItem href="http://www.sutol.cz">sutol.cz</NavItem>
                    <NavItem href="http://www.pradny.com">pradny.com</NavItem>
                    <NavDropdown title="Resources" id="basic-nav-dropdown">
                        <MenuItem href="https://facebook.github.io/react/">React.js</MenuItem>
                        <MenuItem href="http://lesscss.org/">Less</MenuItem>
                        <MenuItem href="http://eslint.org/">ESLint</MenuItem>
                        <MenuItem href="https://babeljs.io/">Babel</MenuItem>
                        <MenuItem href="https://webpack.github.io/">Webpack</MenuItem>
                        <MenuItem divider />
                        <MenuItem href="https://guedebyte.wordpress.com/2016/03/26/building-nsf-using-the-maven-headlessdesigner-plugin-from-openntf/">Maven Headless Designer plugin</MenuItem>
                        <MenuItem href="https://github.com/camac/BuildXPages">Ant Headless Designer tasks</MenuItem>
                        <MenuItem divider />
                        <MenuItem href="https://jenkins.io/">Jenkins</MenuItem>
                        <MenuItem href="https://jenkins.io/doc/book/pipeline/">Jenkins Pipeline</MenuItem>
                        <MenuItem href="http://testng.org/doc/index.html">TestNG</MenuItem>
                        <MenuItem href="http://docs.seleniumhq.org/">Selenium</MenuItem>
                        <MenuItem divider />
                        <MenuItem href="http://code.visualstudio.com/">Visual Studio Code</MenuItem>
                    </NavDropdown>
                </Nav>
            </Navbar>
        )
    }
    render() {
        return (

            <div>
                {this.renderNavbar()}
                <div className="container">
                    <h2>Presentation checklist</h2>
                    <CreateTodoContainer />
                    <VisibleTodosList />
                </div>
            </div>
        );
    }


}

App.propTypes = {
    dispatch: React.PropTypes.func.isRequired
}

export default connect()(App)