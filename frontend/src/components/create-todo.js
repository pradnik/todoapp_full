import React from 'react';
import { Button } from 'react-bootstrap';

export default class CreateTodo extends React.Component {

    render() {

        return (
            <div className="row" style={{marginBottom: 1 + 'em'}}>
                <form onSubmit={this.handleCreate.bind(this)}>
                    <div className="col-md-8">
                        <input className="form-control newInput" type="text" placeholder="What do I need to do?" ref="createInput" />
                    </div>
                    <div className="col-md-4">
                        <Button bsStyle="primary" onClick={this.handleCreate.bind(this)}>Create</Button>
                    </div>
                </form>
            </div>
        );
    }

    handleCreate(event) {
        event.preventDefault();
        this.props.onSubmit(this.refs.createInput.value);
        this.refs.createInput.value = "";
    }

}

CreateTodo.propTypes = {
    onSubmit: React.PropTypes.func.isRequired
}