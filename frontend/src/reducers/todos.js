import {RECEIVE_TODOS} from '../actions/rest'

const todo = (state = {}, action) => {
  switch (action.type) {
    case 'ADD_TODO':
      return {
        id: action.id,
        text: action.text,
        completed: false,
        isEditing: false
      }
    case 'EDIT_TODO':
      if (state.id !== action.id) {
        return state
      }
      return Object.assign({}, state, {
        isEditing: true
      })
    case 'SAVE_TODO':
      if (state.id !== action.id) {
        return state
      }
      return Object.assign({}, state, {
        text: action.text,
        isEditing:false
      })
    case 'CANCEL_EDIT_TODO':
      if (state.id !== action.id) {
        return state
      }
      return Object.assign({}, state, {
        isEditing: false
      })
    case 'TOGGLE_TODO':
      if (state.id !== action.id) {
        return state
      }

      return Object.assign({}, state, {
        completed: !state.completed
      })

    default:
      return state
  }
}

const todos = (state = [], action) => {
  switch (action.type) {
    case 'ADD_TODO':
      return [
        ...state,
        todo(undefined, action)
      ]
    case 'EDIT_TODO':
      return state.map(t =>
        todo(t, action)
      )
    case 'SAVE_TODO':
      return state.map(t =>
        todo(t, action)
      )
    case 'CANCEL_EDIT_TODO':
      return state.map(t =>
        todo(t, action)
      )
    case 'DELETE_TODO':
      return state.filter(t => t.id !== action.id)
    case 'TOGGLE_TODO':
      return state.map(t =>
        todo(t, action)
      )
    case RECEIVE_TODOS:
      return action.todos  
    default:
      return state
  }
}

export default todos