import React from 'react';
import { render } from 'react-dom';
import App from 'components/app';
import '../css/main.less';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware  } from 'redux';
import todoApp from './reducers';
import reduxThunk from 'redux-thunk'; 

const createStoreWithMiddleware = applyMiddleware(reduxThunk)(createStore);
const store = createStoreWithMiddleware(todoApp, window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__())

render(
    <Provider store={store}>
        <App />
    </Provider>,
     document.getElementById('app')
);