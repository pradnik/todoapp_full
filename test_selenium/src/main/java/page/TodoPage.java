package page;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class TodoPage {
	protected WebDriver driver;
	protected By buttonsLocator=By.xpath("//div[contains(@id,\"buttonsPanelNefunkcnost\")]//button");
	private final static By NEW_TODO_INPUT=By.cssSelector("input.newInput");
	private final static By LAST_TODO=By.xpath("(//div[@class='todoRow'])[last()]");
	private final static By TODO_ROWS=By.cssSelector(".todoRow");
	public TodoPage(WebDriver driver) {
		this.driver=driver;
		
	}
	
	public void enterNewTodo(String text){
		driver.findElement(NEW_TODO_INPUT).sendKeys(text);
	}

	public String getLastTodoText() {
		String text=driver.findElement(LAST_TODO).findElement(By.className("todoText")).getText();
		return text;
	}

	public Integer getTodoCount() {
		return driver.findElements(TODO_ROWS).size();
	}

	public void deleteLastTodo() {
		driver.findElement(LAST_TODO).findElement(By.className("todoDelete")).click();;
		
	}

	public String getLastTodoStyle() {
		String style=driver.findElement(LAST_TODO).findElement(By.className("todoText")).getAttribute("style");
		return style;
	}

	public void togglelastTodo() {
		driver.findElement(LAST_TODO).findElement(By.className("todoText")).click();
	}

}
