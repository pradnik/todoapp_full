package util;

import java.io.File;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.Augmenter;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestResult;
import org.testng.Reporter;
import org.testng.TestListenerAdapter;

public class Screenshot extends TestListenerAdapter {

    

    public void onTestFailure(ITestResult tr) {
    	super.onTestFailure(tr);
        ITestContext context = tr.getTestContext();
//        augmentedDriver = driver);
        WebDriver contextDriver=(WebDriver)context.getAttribute("driver");
        WebDriver driver;
        if (contextDriver instanceof TakesScreenshot){
        	driver=contextDriver;
        }else{
        	driver = new Augmenter().augment(contextDriver);
        }
        String browser = (String) context.getAttribute("browser");
        try {
            File f = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
//            String fname="screenshots/" +browser+ System.currentTimeMillis() + ".png";
            String fname=browser+ System.currentTimeMillis() + ".png";
            FileUtils.copyFile(f, new File(context.getOutputDirectory()+"/"+fname));
            System.out.println("saved screenshot " + fname);
            Reporter.setCurrentTestResult(tr); 
            Reporter.log("<a href='"+ fname + "'> <img src='"+fname + "' height='200' width='auto'/> </a>");
    //        Reporter.log("<a href='"+ context.getSuite().getName() +"/"+ fname + "'> <img src='"+ context.getSuite().getName() +"/"+fname + "' height='200' width='auto'/> </a>");
            Reporter.setCurrentTestResult(null); 
            
        } catch (Exception e) {
        	System.out.println("error generating screenshot: "+e);
        	e.printStackTrace();
        }
        
    }

	public void onTestStart(ITestResult result) {
		// TODO Auto-generated method stub
		
	}

	public void onTestSuccess(ITestResult result) {
		// TODO Auto-generated method stub
		
	}

	public void onTestSkipped(ITestResult result) {
		// TODO Auto-generated method stub
		
	}

	public void onTestFailedButWithinSuccessPercentage(ITestResult result) {
		// TODO Auto-generated method stub
		
	}

	public void onStart(ITestContext context) {
		// TODO Auto-generated method stub
		
	}

	public void onFinish(ITestContext context) {
		// TODO Auto-generated method stub
		
	}
    
}
