package testng;

import static org.testng.Assert.*;
import static org.testng.Assert.fail;

import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.ITestContext;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import page.TodoPage;

public class BasicFlow {

	private String uniqeTxt=new SimpleDateFormat("yyyyMMddHHmmssSSS").format(new Date());
	private WebDriver driver;
	private String browser;
	private String baseUrl;
	private boolean acceptNextAlert = true;
	private StringBuffer verificationErrors = new StringBuffer();

	@BeforeMethod
	@Parameters({ "browser", "baseurl" })
	public void setUp(String browser, String baseurl, ITestContext context) throws Exception {
		this.browser = browser;
		this.baseUrl = baseurl;
		WebDriver basedriver;
		if (browser.equalsIgnoreCase("FF")) {
			System.out.println("Firefox driver will be used");
			System.setProperty("webdriver.gecko.driver", "c:\\Selenium\\geckodriver.exe");
			basedriver = new FirefoxDriver();
		} else if (browser.equalsIgnoreCase("CH")) {
			System.out.println("Chrome driver will be used");
			System.setProperty("webdriver.chrome.driver", "c:\\Selenium\\chromedriver.exe");
			basedriver = new ChromeDriver();
		} else if (browser.equalsIgnoreCase("Remote")) {
			System.out.println("Remote IE driver will be used");
			DesiredCapabilities capability = DesiredCapabilities.internetExplorer();
			basedriver = new RemoteWebDriver(new URL("http://192.168.159.132:4444/wd/hub"), capability);
		} else {
			System.out.println("IE driver will be used");
			System.setProperty("webdriver.ie.driver", "c:\\Selenium\\IEDriverServer.exe");
			basedriver = new InternetExplorerDriver();
		}

		driver = basedriver;
		context.setAttribute("driver", driver);
		context.setAttribute("browser", browser);
		// driver = new EventFiringWebDriver(basedriver);
		// WebDriverEventListener errorListener = new
		// AbstractWebDriverEventListener() {
		// @Override
		// public void onException(Throwable throwable, WebDriver driver) {
		// takeScreenshot("some name");
		// }
		// };
		// driver.register(errorListener);

		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		// System.setProperty("webdriver.chrome.driver",
		// "d:\\ChromeDriver\\chromedriver.exe");
		// driver = new ChromeDriver();
		// driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
	}
	
	@Test
	public void titleCheck() {
		driver.get(baseUrl);
		assertEquals(this.driver.findElement(By.className("navbar-brand")).getText(),"SUTOL 2016 automation demo");

	}
	
	private void createTodo(String txt){
		TodoPage loginPage = new TodoPage(driver);
		loginPage.enterNewTodo(txt+Keys.RETURN);
		//loginPage.submitNewTodo();
		waitFE();
	}
	@Test
	public void createTodo() {
		driver.get(baseUrl);
		TodoPage loginPage = new TodoPage(driver);
		String todoText="Selenium " + uniqeTxt;
		createTodo(todoText);
		assertEquals(loginPage.getLastTodoText(),todoText);

	}


	@Test
	public void deleteLastTodo() {
		String txt=new SimpleDateFormat("yyyyMMddHHmmssSSS").format(new Date());
		driver.get(baseUrl);
		TodoPage loginPage = new TodoPage(driver);
		String todoText="Selenium " + txt;
		createTodo(todoText);
		
		Integer todoCount=loginPage.getTodoCount();
		String lastText=loginPage.getLastTodoText();
		loginPage.deleteLastTodo();
		waitFE();
		todoCount--;
		assertEquals(loginPage.getTodoCount(),todoCount);
		assertNotEquals(loginPage.getLastTodoText(), lastText);
		
	}
	
	@Test
	public void toggleTodo() {
		driver.get(baseUrl);
		TodoPage loginPage = new TodoPage(driver);
		createTodo(uniqeTxt);
		
		String style=loginPage.getLastTodoStyle();
		loginPage.togglelastTodo();
		waitFE();
		assertNotEquals(loginPage.getLastTodoStyle(), style);
	}
	
	private void waitFE(){
		try {
			Thread.sleep(500);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	@AfterMethod
	public void tearDown() throws Exception {
		driver.close();
		try {
			Thread.sleep(2000);
			driver.quit();
		} catch (InterruptedException e1) {
			e1.printStackTrace();
		}
		String verificationErrorString = verificationErrors.toString();
		if (!"".equals(verificationErrorString)) {
			fail(verificationErrorString);
		}
	}
	/*
	private String login() {
		driver.get(baseUrl);
		Login loginPage = new Login(driver);
		Assert.assertTrue(loginPage.isLoginPage(),"Login page not found");
		loginPage.enterUserName("Martin Pradny");
		loginPage.enterPassword("lotusnotes");
		loginPage.submit();
		loginPage.waitToComplete();
		return driver.getCurrentUrl();
	}
	
	@Test
	public void startNewRequest() {
		String url=login();
		Assert.assertTrue(url.contains("adminPortal.xsp"), "Not redirected to adminPortal.xsp after login + " + url);
		AdminPortal adminPortal=new AdminPortal(driver);
		RequestEdit request = adminPortal.startNewRequest("P.1.1 Register User");
		//Sleep to load the form. Should be done differently, but as there is refresh after page load, there is no easy way to wait for it.
		try {
			Thread.sleep(5000); //
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		//new Select(driver.findElement(By.cssSelector(".input_Classification"))).selectByIndex(1);
		
		Map<String,String> itemValues=new LinkedHashMap<>();
		itemValues.put("BCCRegProfile", "APAC - AU - Sika AU - NSW / Pradny");
		itemValues.put("Language","Czech");
		itemValues.put("Classification","Contract Staff");
		itemValues.put("Lastname", "ST " + uniqeTxt);
		itemValues.put("Firstname", "ST " + uniqeTxt);
		
		request.fillItems(itemValues);
		
		
		
		request.switchTab("Additional information");
		itemValues=new LinkedHashMap<>();
		itemValues.put("JobTitle", "Test");
		itemValues.put("Department", "Test");
		itemValues.put("EmployeeID", "Test");
		itemValues.put("Location", "Test");
		itemValues.put("Manager", "Tester");
		request.fillItems(itemValues);
		
		//hack to support name picker
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		driver.findElement(By.cssSelector(".select2-results__option")).click();
		//name picker end
		
		request.saveAndRelease();
		
		 Wait<WebDriver> wait = new WebDriverWait(driver,10);
			wait.until(ExpectedConditions.urlContains("adminPortal.xsp"));
	}
	*/
	
}
